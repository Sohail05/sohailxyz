---
title: Blog
blog_url: blog
body_classes: header-image fullwidth
sitemap:
    changefreq: monthly
    priority: 1.03

content:
    items: @self.children
    order:
        by: date
        dir: desc
    limit: 5
    pagination: true

feed:
    description: Sohail XYZ Blog
    limit: 10

pagination: true
---

# I'm Sohail, what's up!
## A **software developer's** blog.
